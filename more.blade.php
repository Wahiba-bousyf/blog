@extends('blog.navbar')
<style>
    .card-container {
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
    }

    .card {
        position: relative;
        flex: 0 0 calc(33.33% - 20px);
        margin: 10px;
        border-radius: 8px;
        box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        padding: 20px;
        background-color: #fff;
        max-width: 400px;
    }
    img{
        width: 100%; /* Pour que l'image prenne toute la largeur de la carte */
        height: auto; /* Pour conserver le ratio hauteur/largeur de l'image */
        border-radius: 8px 8px 0 0; /* Pour donner un coin arrondi à l'image uniquement en haut */
    }
    .card h1 {
        font-size: 24px;
        margin-bottom: 10px;
    }

    .card h5 {
        font-size: 16px;
        margin-bottom: 15px;
        color: #666;
    }

    .card p {
        font-size: 14px;
        line-height: 1.6;
        color: #333;
    }
    .titre{
        margin-top: 7%;
    }
    .card-buttons {
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;
        padding: 10px;
        background-color: rgba(255, 255, 255, 0.9);
        border-radius: 0 0 8px 8px;
        display: flex;
        justify-content: space-around;
    }

    .card-buttons button {
        padding: 8px 16px;
        border: none;
        border-radius: 4px;
        background-color: #007bff;
        color: #fff;
        cursor: pointer;
        transition: background-color 0.3s;
    }

    .card-buttons button:hover {
        background-color: #0056b3;
    }
    .alert {
    padding: 15px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px;
}

.alert-success {
    color: #155724;
    background-color: #d4edda;
    border-color: #c3e6cb;
}

</style>

@section('contenu')
<div class="container">
<h1 class="titre">les posts</h1>
@if (session()->has('success'))
<div class="alert alert-success">
    {{ session()->get('success')}}
</div>
@endif
@if(session()->has('message'))
    <div class="alert alert-{{ session('alert-type') }}">
        {{ session()->get('message') }}
    </div>
@endif
<div class="card-container">
    @isset($posts)
    @foreach ($posts as $post)
    <div class="card">
        <img src="{{asset('./uploads/'.$post->file)}}" alt="" />
        <h1>{{$post->email}}</h1>
        <h3>{{$post->user ? $post->user->name : null}}</h3>
        <p>{{$post->adress}}</p>
        <p>{{$post->city}}</p><br/><br/>
        <div class="card-buttons">
            <a href="{{route('detaills',$post->id)}}"><button style="background-color: rgb(88, 88, 252)">more</button></a>
            @if (auth()->check() && (auth()->user()->id ==$post->user_id || auth()->user()->isAdmin))
            <a href="{{route('edit',$post->id)}}"><button style="background-color: #28a745">modifier</button></a>
            <form id={{$post->id}} action="{{route('delete',$post->id)}}" method="post">
                @csrf
                @method('delete')
                <button onclick="event.preventDefault(); if(confirm('etes vous daccorde pour la supprission?')) document.getElementById({{$post->id}}).submit();" style="background-color: red" type="submit">delete</button>
            </form>
            @endif
        </div>
    </div>
@endforeach
</div>
    <div class="d-flex-justify-content-center my-4">
    {{$posts->links()}}
    </div>
@endsection
    @endisset

</div>
