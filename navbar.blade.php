<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Document</title>
</head>
<style>
    body {
        font: 400 15px Lato, sans-serif;
        line-height: 1.8;
        color: #818181;
    }
    .carousel-indicators li {
        border-color: rgb(143, 143, 67);
    }
    .carousel-indicators li.active {
        background-color: rgb(143, 143, 67);
    }
    .navbar {
        margin-bottom: 0;
        background-color: rgb(143, 143, 67);
        z-index: 9999;
        border: 0;
        font-size: 14px !important;
        line-height: 1.42857143 !important;
        letter-spacing: 4px;
        border-radius: 0;
        font-family: Montserrat, sans-serif;
    }
    .navbar li a, .navbar .navbar-brand {
        color: #fff !important;
    }
    .navbar-nav li a:hover, .navbar-nav li.active a {
        color: rgb(143, 143, 67) !important;
        background-color: #fff !important;
        height: 65px;
    }
    .navbar-default .navbar-toggle {
        border-color: transparent;
        color: #fff !important;
    }
    .img1{
        width: 200px;
        margin-top:-55px;
    }
</style>
<body>
    <nav class="navbar navbar-default navbar-fixed-top" style="height:65px">
        <div class="container">
            <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <img src="light-removebg-preview.png" class="img1">
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{route('index')}}">ABOUT</a></li>
                <li><a href="{{route('index')}}">PROFIL</a></li>
                <li><a href="{{route('show')}}">POSTS</a></li>
                @if(auth()->check())
            <li><a href="{{route('create')}}">CREATE</a></li>
            <li><a href="{{route('profile.show')}}">{{auth()->user()->name}}</a></li>
            @else
            <li><a href="{{route('register')}}">REGISTRE</a></li>
            <li><a href="{{route('login')}}">LOG IN</a></li>
            @endif
            </ul>
            </div>
        </div>
        </nav>
        @yield('contenu')
</body>
</html>
