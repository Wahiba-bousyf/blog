<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BlogRequest;
use App\Models\Post;

class blogController extends Controller
{
    public function index()
    {
        return view('blog.master');
    }
    public function create()
    {
        if(auth()->check()){
            return view('blog.create');
        }
        else {
            return view('auth.login');
        }
        return view('blog.create');
    }
    public function store(BlogRequest $request)
    {
        if($request->has('file')){
            $file=$request->file;
            $image_n=time().'_'.$file->getClientOriginalName();
            $file->move(public_path('uploads'),$image_n);
        }
        Post::create([
            'email'=>$request->email,
            'number'=>$request->number,
            'adress'=>$request->adress,
            'file'=>$image_n,
            'city'=>$request->city,
            'state'=>$request->state,
            'zip'=>$request->zip,
            'user_id'=>auth()->user()->id,
        ]);
        return redirect()->route('show')->with([
            'success'=>'le post est bien ajouter'
        ]);
    }
    public function show()
    {
        $posts=Post::latest()->paginate(3);
        return view('blog.more')->with([
            'posts'=>$posts
        ]);
    }
    public function detaills($id)
    {
        if(auth()->check()){
        $post = Post::where('id', $id)->first();
        return view('blog.detaills')->with([
            'post' => $post
        ]);
    }
    else {
        return view('auth.login');
    }
    }
    public function searche(Request $request)
        {
            $posts = Post::where('city', $request->ville)->get();
            if ($posts->isEmpty()) {
                return redirect()->back()->with([
                    'alert-type' => 'error',
                    'message' => 'La ville n\'existe pas.'
                ]);
            } else {
                return redirect()->route('show')->with([
                    'alert-type' => 'success',
                    'message' => 'La ville existe.'
                ]);
            }
        }
    public function edit($id)
    {
        if(auth()->check()){
            $post = Post::where('id', $id)->first();
            return view('blog.edit')->with([
                'post' => $post
            ]);
        }
        else {
            return view('auth.login');
        }
    }
    public function update(BlogRequest $request,$id)
    {
        $post=Post::where('id',$id)->first();
        if($request->has('file')){
            $file=$request->file;
            $image_n=time().'_'.$file->getClientOriginalName();
            $file->move(public_path('uploads'),$image_n);
            $post->file=$image_n;
        }
        $post->update([
            'email'=>$request->email,
            'file'=>$post->file,
            'adress'=>$request->adress,
            'city'=>$request->city,
            'zip'=>$request->zip,
            'number'=>$request->number,
            'state'=>$request->state,
            'user_id'=>auth()->user()->id,
        ]);
        return redirect()->route('show')->with([
            'success'=>'le post est modifier'
        ]);
    }
    public function delete($id)
    {
        $post=Post::where('id',$id)->first();
        $post->delete();
        return redirect()->route('show')->with([
            'success'=>'le post est suprimer'
        ]);
    }
}
