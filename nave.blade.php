    <!DOCTYPE html>
    <html lang="en">
    <head>
    <title>WahibaWeb</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
    body {
        font: 400 15px Lato, sans-serif;
        line-height: 1.8;
        color: #818181;
    }
    h2 {
        font-size: 24px;
        text-transform: uppercase;
        color: #303030;
        font-weight: 600;
        margin-bottom: 30px;
    }
    h4 {
        font-size: 19px;
        line-height: 1.375em;
        color: #303030;
        font-weight: 400;
        margin-bottom: 30px;
    }
    #inp1{
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .jumbotron {
        background-color: rgb(143, 143, 67);
        /* background-image: url(hand-painted-watercolor-pastel-sky-background_23-2148902023.avif); */
        color: black;
        background-repeat: no-repeat;
        background-size: cover;
        padding: 220px 50px;
        font-family: Montserrat, sans-serif;
    }
    .container-fluid {
        padding: 60px 50px;
    }
    .bg-grey {
        background-color: #f6f6f6;
    }
    .logo-small {
        color: rgb(143, 143, 67);
        font-size: 50px;
    }
    .logo {
        color: rgb(143, 143, 67);
        font-size: 200px;
    }
    .thumbnail {
        padding: 0 0 15px 0;
        border: none;
        border-radius: 0;
    }
    .thumbnail img {
        width: 100%;
        height: 100%;
        margin-bottom: 10px;
    }
    .carousel-control.right, .carousel-control.left {
        background-image: none;
        color: rgb(143, 143, 67);
    }
    .carousel-indicators li {
        border-color: rgb(143, 143, 67);
    }
    .carousel-indicators li.active {
        background-color: rgb(143, 143, 67);
    }
    .item h4 {
        font-size: 19px;
        line-height: 1.375em;
        font-weight: 400;
        font-style: italic;
        margin: 70px 0;
    }
    .item span {
        font-style: normal;
    }
    .input-group .form-control {
    position: relative;
    z-index: 2;
    float: RIGHT;
    width: 60rem;
    margin-bottom: 0;
}
    .panel {
        border: 1px solid rgb(143, 143, 67);
        border-radius:0 !important;
        transition: box-shadow 0.5s;
    }
    .panel:hover {
        box-shadow: 5px 0px 40px rgba(0,0,0, .2);
    }
    .panel-footer .btn:hover {
        border: 1px solid rgb(143, 143, 67);
        background-color: #fff !important;
        color: rgb(143, 143, 67);
    }
    .panel-heading {
        color: #fff !important;
        background-color: rgb(143, 143, 67) !important;
        padding: 5px;
        border-bottom: 1px solid transparent;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 40px;
        border-bottom-right-radius: 40px;
    }
    .panel-footer {
        background-color: white !important;
    }
    .panel-footer h3 {
        font-size: 32px;
    }
    .panel-footer h4 {
        color: #aaa;
        font-size: 14px;
    }
    .panel-footer .btn {
        margin: 15px 0;
        background-color: rgb(143, 143, 67);
        color: #fff;
    }
    .navbar {
        margin-bottom: 0;
        background-color: rgb(143, 143, 67);
        z-index: 9999;
        border: 0;
        font-size: 14px !important;
        line-height: 1.42857143 !important;
        letter-spacing: 4px;
        border-radius: 0;
        font-family: Montserrat, sans-serif;
    }
    .navbar li a, .navbar .navbar-brand {
        color: #fff !important;
    }
    .navbar-nav li a:hover, .navbar-nav li.active a {
        color: rgb(143, 143, 67) !important;
        background-color: #fff !important;
        height: 65px;
    }
    .navbar-default .navbar-toggle {
        border-color: transparent;
        color: #fff !important;
    }
    footer .glyphicon {
        font-size: 20px;
        margin-bottom: 20px;
        color: rgb(143, 143, 67);
    }
    .slideanim {visibility:hidden;}
    .slide {
        animation-name: slide;
        -webkit-animation-name: slide;
        animation-duration: 1s;
        -webkit-animation-duration: 1s;
        visibility: visible;
    }
    .jumbotron p {
    margin-bottom: 15px;
    font-size: 16px;
    font-weight: 200;
    color: black;
}
    @keyframes slide {
        0% {
        opacity: 0;
        transform: translateY(70%);
        }
        100% {
        opacity: 1;
        transform: translateY(0%);
        }
    }
    @-webkit-keyframes slide {
        0% {
        opacity: 0;
        -webkit-transform: translateY(70%);
        }
        100% {
        opacity: 1;
        -webkit-transform: translateY(0%);
        }
    }

    @media screen and (max-width: 768px) {
        .col-sm-4 {
        text-align: center;
        margin: 25px 0;
        }
        .btn-lg {
        width: 100%;
        margin-bottom: 35px;
        }
    }
    @media screen and (max-width: 480px) {
        .logo {
        font-size: 150px;
        }
    }
    .img1{
        width: 200px;
        margin-top:-55px;
    }
    </style>
    </head>
    <body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
    <nav class="navbar navbar-default navbar-fixed-top" style="height:65px">
    <div class="container">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <img src="light-removebg-preview.png" class="img1">
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="#about">ABOUT</a></li>
            <li><a href="#services">PROFIL</a></li>
            <li><a href="{{route('show')}}">POSTS</a></li>
            <li><a href="#contact">CONTACT</a></li>
            @if(auth()->check())
            <li><a href="{{route('create')}}">CREATE</a></li>
            <li><a href="{{route('profile.show')}}">{{auth()->user()->name}}</a></li>
            @else
            <li><a href="{{route('register')}}">REGISTRE</a></li>
            <li><a href="{{route('login')}}">LOG IN</a></li>
            @endif
        </ul>
        </div>
    </div>
    </nav>
    <div class="jumbotron text-center">
        <h1>WahibaWeb</h1>
        <p class="p1">
        Welcome to WahibaWeb, the ultimate destination for connecting with friends, sharing your stories, and engaging with the world. With our innovative features and user-friendly interface, we're redefining the way you experience social networking.
        </p>
        <form id="inp1" action="{{route('searche')}}" method="get">
                <div class="input-group">
                    <input type="text" class="form-control" name="ville" placeholder="Settat" required>
                    <div class="input-group-btn">
                        <button type="input" class="btn" style="background-color: rgb(255, 255, 61)">Subscribe</button>
                    </div>
                </div>
            </form><br>
            @if(session()->has('message'))
            <div class="alert alert-{{ session('alert-type') }}" style="background-color: rgb(83, 206, 236)">
                {{ session()->get('message') }}
            </div>
        @endif
    </div>
    <div id="about" class="container-fluid">
    <div class="row">
        <div class="col-sm-8">
        <h2>About Company Page</h2><br>
        <h4>At WAHIBAWEB, we are dedicated to delivering innovative and effective digital solutions that empower businesses to thrive in the online world. With a team of experienced professionals, we pride ourselves on our commitment to excellence, creativity, and customer </h4><br>
        <p> we strive to exceed expectations and deliver results that propel your business forward. Our commitment to excellence, coupled with a passion for cutting-edge technology, ensures that we remain at the forefront of the digital landscape. Partner with us to transform your vision into reality and unlock the full potential of your online presence</p>
        <br><button class="btn btn-default btn-lg">Get in Touch</button>
        </div>
        <div class="col-sm-4">
        <img src="info.png" style="width: 380px" alt="">
        </div>
    </div>
    </div>

    <div class="container-fluid bg-grey">
    <div class="row">
        <div class="col-sm-4">
        <img src="diamond.png" style="width:350px" alt="">
        </div>
        <div class="col-sm-8">
        <h2>Our Values</h2><br>
        <h4><strong>Values:</strong> At WAHIBAWEB our values are the cornerstone of everything we do. We are committed to integrity, transparency, and excellence in every aspect of our work. Customer satisfaction is our priority, and we strive to build lasting relationships based on trust and mutual respect</h4><br>
        <p><strong>VISION:</strong>We value creativity, innovation, and continuous improvement, constantly seeking new ways to exceed our clients' expectations. With a focus on collaboration and teamwork, we foster an environment where every idea is valued, and every individual is empowered to reach their full potential. At WAHIBAWEB, we believe that by upholding these values we can make a positive impact and achieve success together."</p>
        </div>
    </div>
    </div>

    <!-- Container (Services Section) -->
    <div id="services" class="container-fluid text-center">
    <h2>SERVICES</h2>
    <h4>What we offer</h4>
    <br>
    <div class="row slideanim">
        <div class="col-sm-4">
            <img src="customer-service (1).png" style="width: 60px" alt="">
        <h4>OPEN SERVICE </h4>
        <p>We are a passionate team of web professionals</p>
        </div>
        <div class="col-sm-4">
            <img src="website.png" style="width: 50px" alt="">
        <h4>OPEN SOURCE</h4>
        <p>We are a passionate team of web professionals</p>
        </div>
        <div class="col-sm-4">
        <img src="certificate.png" style="width: 50px" alt="">
        <h4>CERTIFIED</h4>
        <p>We are a passionate team of web professionals</p>
        </div>
    </div>
    <br><br>
    <div class="row slideanim">
        <div class="col-sm-4">
        <img src="shutdown.png" style="width: 50px" alt="">
        <h4>ALL THE TIME</h4>
        <p>We are a passionate team of web professionals</p>
        </div>
        <div class="col-sm-4">
            <img src="burden.png" style="width: 50PX" alt="">
        <h4>HARD WORK</h4>
        <p>We are a passionate team of web professionals</p>
        </div>
        <div class="col-sm-4">
        <img src="free.png" style="width: 50px" alt="">
        <h4 style="color:#303030;">FREE SERVICE</h4>
        <p>We are a passionate team of web professionals</p>
        </div>
    </div>
    </div>

    <!-- Container (Portfolio Section) -->
    <div id="portfolio" class="container-fluid text-center bg-grey">
    <h2>Portfolio</h2><br>
    <h4>What we have created</h4>
    <div class="row text-center slideanim">
        <div class="col-sm-4">
        <div class="thumbnail">
            <img src="istockphoto-1460896518-170667a.webp" alt="Paris" width="400" height="300">
            <p><strong>Amine benani</strong></p>
            <p>Yes, we built Paris</p>
        </div>
        </div>
        <div class="col-sm-4">
        <div class="thumbnail">
            <img src="140588054-image-d-une-jeune-femme-brune-se-demandant-et-regardant-la-caméra-isolée-sur-fond-rose.jpg" alt="New York" width="400" height="300">
            <p><strong>Laila naji</strong></p>
            <p>We built New York</p>
        </div>
        </div>
        <div class="col-sm-4">
        <div class="thumbnail">
            <img src="jeune-belle-femme-pull-chaud-rose-aspect-naturel-souriant-portrait-isole-cheveux-longs_285396-896.avif" alt="San Francisco" width="400" height="300">
            <p><strong>Ikhlas boujdi</strong></p>
            <p>Yes, San Fran is ours</p>
        </div>
        </div>
    </div><br>

    <h2>What our customers say</h2>
    <div id="myCarousel" class="carousel slide text-center" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
        <div class="item active">
            <h4>"This company is the best. I am so happy with the result!"<br><span>Michael Roe, Vice President, Comment Box</span></h4>
        </div>
        <div class="item">
            <h4>"One word... WOW!!"<br><span>John Doe, Salesman, Rep Inc</span></h4>
        </div>
        <div class="item">
            <h4>"Could I... BE any more happy with this company?"<br><span>Chandler Bing, Actor, FriendsAlot</span></h4>
        </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
        </a>
    </div>
    </div>

    <!-- Container (Pricing Section) -->
    <div id="pricing" class="container-fluid">
    <div class="text-center">
        <h2>Pricing</h2>
        <h4>Choose a payment plan that works for you</h4>
    </div>
    <div class="row slideanim">
        <div class="col-sm-4 col-xs-12">
        <div class="panel panel-default text-center">
            <div class="panel-heading">
            <h1>Basic</h1>
            </div>
            <div class="panel-body">
            <p><strong>2000</strong> for begein integer insud</p>
            <p><strong>1500</strong> for begein integer insud</p>
            <p><strong>500</strong> for begein integer insud</p>
            <p><strong>200</strong> for begein integer insud</p>
            <p><strong>Endless</strong> for begein integer insud</p>
            </div>
            <div class="panel-footer">
            <h3>$19</h3>
            <h4>per month</h4>
            <button class="btn btn-lg">Sign Up</button>
            </div>
        </div>
        </div>
        <div class="col-sm-4 col-xs-12">
        <div class="panel panel-default text-center">
            <div class="panel-heading">
            <h1>Pro</h1>
            </div>
            <div class="panel-body">
                <p><strong>2000</strong> for begein integer insud</p>
                <p><strong>1500</strong> for begein integer insud</p>
                <p><strong>500</strong> for begein integer insud</p>
                <p><strong>200</strong> for begein integer insud</p>
                <p><strong>Endless</strong> for begein integer insud</p>
            </div>
            <div class="panel-footer">
            <h3>$29</h3>
            <h4>per month</h4>
            <button class="btn btn-lg">Sign Up</button>
            </div>
        </div>
        </div>
        <div class="col-sm-4 col-xs-12">
        <div class="panel panel-default text-center">
            <div class="panel-heading">
            <h1>Premium</h1>
            </div>
            <div class="panel-body">
                <p><strong>2000</strong> for begein integer insud</p>
                <p><strong>1500</strong> for begein integer insud</p>
                <p><strong>500</strong> for begein integer insud</p>
                <p><strong>200</strong> for begein integer insud</p>
                <p><strong>Endless</strong> for begein integer insud</p>
            </div>
            <div class="panel-footer">
            <h3>$49</h3>
            <h4>per month</h4>
            <button class="btn btn-lg">Sign Up</button>
            </div>
        </div>
        </div>
    </div>
    </div>

    <!-- Container (Contact Section) -->
    <div id="contact" class="container-fluid bg-grey">
    <h2 class="text-center">CONTACT</h2>
    <div class="row">
        <div class="col-sm-5">
        <p>Contact us and we'll get back to you within 24 hours.</p>
        <p><span class="glyphicon glyphicon-map-marker"></span> Settat</p>
        <p><span class="glyphicon glyphicon-phone"></span> 0635066827</p>
        <p><span class="glyphicon glyphicon-envelope"></span>wahibabousyf@gmail.com</p>
        </div>
        <div class="col-sm-7 slideanim">
        <div class="row">
            <div class="col-sm-6 form-group">
            <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>
            </div>
            <div class="col-sm-6 form-group">
            <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
            </div>
        </div>
        <textarea class="form-control" id="comments" name="comments" placeholder="Comment" rows="5"></textarea><br>
        <div class="row">
            <div class="col-sm-12 form-group">
            <button class="btn btn-default pull-right" type="submit">Send</button>
            </div>
        </div>
        </div>
    </div>
    </div>
    <script>
    $(document).ready(function(){
    $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
        if (this.hash !== "") {
        event.preventDefault();
        var hash = this.hash;
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 900, function(){
            window.location.hash = hash;
        });
        }
    });

    $(window).scroll(function() {
        $(".slideanim").each(function(){
        var pos = $(this).offset().top;

        var winTop = $(window).scrollTop();
            if (pos < winTop + 600) {
            $(this).addClass("slide");
            }
        });
    });
    })
    </script>

    </body>
    </html>
