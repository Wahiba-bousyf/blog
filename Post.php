<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $tabke='blog';
    protected $fillable=['email','number','adress','file','city','state','zip','user_id'];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
