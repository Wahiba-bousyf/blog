@extends('blog.navbar')
@section('contenu')
<div class="container" style="margin-top: 8%">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{route('store')}}" method="post" enctype="multipart/form-data">
    @csrf
        <div class="form-row">
        <div class="form-group col-md-6">
            <label for="inputEmail4">Email</label>
            <input type="email" class="form-control" name="email" id="inputEmail4" placeholder="Email">
        </div>
        <div class="form-group col-md-6">
            <label for="inputPassword4">Number</label>
            <input type="number" class="form-control" name="number" id="inputPassword4" placeholder="06 35 06 68 27">
        </div>
        </div>
        <div class="form-group col-md-12">
        <label for="inputAddress">Address</label>
        <input type="text" class="form-control" name="adress" id="inputAddress" placeholder="1234 Main St">
        </div>
        <div class="form-group col-md-12">
        <label for="inputAddress2">File</label>
        <input type="file" class="form-control" name="file" id="inputAddress2" placeholder="Apartment, studio, or floor">
        </div>
        <div class="form-row">
        <div class="form-group col-md-6">
            <label for="inputCity">City</label>
            <input type="text" class="form-control" name="city" id="inputCity">
        </div>
        <div class="form-group col-md-4">
            <label for="inputState">State</label>
            <select id="inputState" name="state" class="form-control">
            <option selected>Choose...</option>
            <option>first option</option>
            <option>secend option</option>
            <option>final option</option>
            </select>
        </div>
        <div class="form-group col-md-2">
            <label for="inputZip">Zip</label>
            <input type="text" name="zip" class="form-control" id="inputZip">
        </div>
        </div>
        <div class="form-group col-md-12">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" id="gridCheck">
            <label class="form-check-label" for="gridCheck">
            Nouveaux etat
            </label>
        </div>
    </div>
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary">Sign in</button>
    </div>
</div>
</form>
@endsection
