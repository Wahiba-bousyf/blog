<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'email'=>'required|min:5|max:40',
            'number'=>'required|min:10|max:30',
            'adress'=>'required|min:5|max:40',
            'file'=>'required|mimes:png,jpg,jpeg',
            'city'=>'required|min:3|max:20',
            'state'=>'required|min:2|max:40',
            'zip'=>'required|min:3|max:40',
        ];
    }
}
