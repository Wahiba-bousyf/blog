<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\blogController;


Route::get('/',[blogController::class,'index'])->name('index');
Route::get('/create',[blogController::class,'create'])->name('create');
Route::post('/store',[blogController::class,'store'])->name('store');
Route::get('/show',[blogController::class,'show'])->name('show');
Route::delete('/delete{id}',[blogController::class,'delete'])->name('delete');
Route::get('/edit{id}',[blogController::class,'edit'])->name('edit');
Route::put('/update{id}',[blogController::class,'update'])->name('update');
Route::get('/searche',[blogController::class,'searche'])->name('searche');
Route::get('/detaills{id}',[blogController::class,'detaills'])->name('detaills');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {return view('blog.nave');});
});
