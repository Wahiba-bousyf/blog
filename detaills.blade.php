@extends('blog.navbar')
@section('contenu')
    <style>
        .card {
            width: 60%;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 8px;
            box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
            background-color: #fff;
        }

        .card img {
            width: 100%;
            border-radius: 8px;
            margin-bottom: 15px;
        }

        .card h1 {
            font-size: 24px;
            margin-bottom: 10px;
        }

        .card h3 {
            font-size: 18px;
            color: #666;
            margin-bottom: 5px;
        }

        .card p {
            font-size: 16px;
            color: #333;
            margin-bottom: 15px;
        }

        .card-buttons {
            text-align: center;
        }

        .card-buttons button {
            padding: 10px 20px;
            font-size: 18px;
            background-color: #007bff;
            color: #fff;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }

        .card-buttons button:hover {
            background-color: #0056b3;
        }
        .tit{
            margin-left: 20%;
        }
    </style>
    <br><br><br><br>
    <h1 class="tit">Details de post de {{$post->user ? $post->user->name : null}} </h1>
    <div class="card">
        <img src="{{ asset('./uploads/'.$post->file) }}" alt="">
        <h1>Email :{{ $post->email }}</h1>
        <h3>Owner :{{ $post->user ? $post->user->name : null }}</h3>
        <p>Adress :{{ $post->adress }}</p>
        <p>City   :{{ $post->city }}</p>
        <p>Number :{{ $post->number }}</p>
        <p>State  :{{ $post->state }}</p>
        <p>Zip    :{{ $post->zip }}</p>
        <div class="card-buttons">
            <a href="{{ route('show') }}"><button>BACK</button></a>
        </div>
    </div>
@endsection
